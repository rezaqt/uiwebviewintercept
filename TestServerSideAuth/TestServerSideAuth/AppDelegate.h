//
//  AppDelegate.h
//  TestServerSideAuth
//
//  Created by Syah Riza on 1/29/14.
//  Copyright (c) 2014 Kii. All rights reserved.
//

#import <UIKit/UIKit.h>
/** Test Class
 
 */
@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
/**Test
* 
* &nbsp;&nbsp;&nbsp;&nbsp;indent aaa
 */
- (void) testMethod;
@end
