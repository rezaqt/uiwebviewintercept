//
//  ViewController.m
//  TestServerSideAuth
//
//  Created by Syah Riza on 1/29/14.
//  Copyright (c) 2014 Kii. All rights reserved.
//

#import "ViewController.h"
//#import <KiiSDK/Kii.h>


@interface ViewController (){
    BOOL _sessionChecked;
}

@end

@implementation ViewController

//- (void) testCallAPI
//{
//
//    [KiiSocialConnect logIn:kiiSCNConnector
//               usingOptions:@{@"provider":@(kiiConnectorFacebook)}
//               withDelegate:self
//                andCallback:@selector(socialLoggedInWithUser:andNetwork:andError:)];
//}
//
//- (void) socialLoggedInWithUser: (KiiUser*) user andNetwork:(KiiSocialNetworkName) network andError : (NSError*) error
//{
//    if(error==nil){
//        NSDictionary* tokenDict = [KiiSocialConnect getAccessTokenDictionaryForNetwork:kiiSCNConnector];
//        NSString* token = tokenDict[@"oauth_token"];
//        NSString* tokenSecret = tokenDict[@"oauth_token_secret"];
//        NSString* providerUserId = tokenDict[@"provider_user_id"];
//    }
//}

- (void)viewDidLoad
{
    [super viewDidLoad];
    
	NSString *homeDir = [[NSBundle mainBundle] resourcePath];
    NSString *filePath = [homeDir stringByAppendingPathComponent:@"sandbox.plist"];
    
    NSFileManager *fileManager = [NSFileManager defaultManager];
    if (![fileManager fileExistsAtPath:filePath]) {
        NSLog(@"There is no plist : %@", @"sandbox");
        
    }
    NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    NSMutableString* urlStr=[NSMutableString string];
    [urlStr appendFormat:@"%@/apps/%@/integration/webauth/connect?id=twitter",dict[@"BaseURL"],dict[@"AppID"]];
    NSLog(@"%@",urlStr);
    NSURLRequest* request = [NSURLRequest requestWithURL:[NSURL URLWithString:urlStr]];
    [self.webView loadRequest:request];
    _sessionChecked = NO;
    
}
- (BOOL)webView:(UIWebView *)webView shouldStartLoadWithRequest:(NSURLRequest *)request navigationType:(UIWebViewNavigationType)navigationType{
    NSString *filePath = [[[NSBundle mainBundle] resourcePath] stringByAppendingPathComponent:@"sandbox.plist"];
    NSDictionary* dict = [NSDictionary dictionaryWithContentsOfFile:filePath];
    
    NSString* url = request.URL.absoluteString;
    if (![url hasPrefix:[NSString stringWithFormat:@"%@/apps/%@/integration/webauth/",dict[@"BaseURL"],dict[@"AppID"]]]&&!_sessionChecked) {
        NSURLConnection *urlConnection = [NSURLConnection connectionWithRequest:request delegate:self];
        [urlConnection start];
        return NO;
    }
    
    return YES;
}
- (void) connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response
{
    if ([response isKindOfClass:[NSHTTPURLResponse class]]) {
        
        NSHTTPURLResponse *httpResponse = (NSHTTPURLResponse *) response;
        int status = [httpResponse statusCode];
        NSLog(@"http status :%d",status);
        if (status>=200&&status<300) {
            [self.webView loadRequest:connection.currentRequest];
            _sessionChecked = YES;
        }
    }
}
- (void)didReceiveMemoryWarning
{
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)webViewDidStartLoad:(UIWebView *)webView{
    NSString* url = webView.request.URL.absoluteString;
    NSLog(@"%@",url);
}
- (void)webViewDidFinishLoad:(UIWebView *)webView{
    //NSLog(@"%@",webView.);
    NSString* url = webView.request.URL.absoluteString;
    NSLog(@"%@",url);
    _sessionChecked = NO;
}
- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error{
    NSLog(@"%@",error);
}
@end
