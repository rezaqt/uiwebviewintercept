//
//  ViewController.h
//  TestServerSideAuth
//
//  Created by Syah Riza on 1/29/14.
//  Copyright (c) 2014 Kii. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController<UIWebViewDelegate>
@property(nonatomic,weak) IBOutlet UIWebView* webView;
@end
