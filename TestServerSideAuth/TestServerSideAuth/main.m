//
//  main.m
//  TestServerSideAuth
//
//  Created by Syah Riza on 1/29/14.
//  Copyright (c) 2014 Kii. All rights reserved.
//

#import <UIKit/UIKit.h>

#import "AppDelegate.h"

int main(int argc, char * argv[])
{
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
